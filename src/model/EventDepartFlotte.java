package model;

import java.util.function.BiConsumer;

import utils.Log;

/**
 * 
 * @author samuel
 *
 */
public class EventDepartFlotte implements BiConsumer<Integer, EventManager> {

	private final Service _service;
	private final Terminal _target;

	public EventDepartFlotte(Service _service, Terminal _target) {
		super();
		this._service = _service;
		this._target = _target;
	}

	@Override
	public void accept(Integer t, EventManager u) {

		Log.println("Service " + _service.getServiceNum() + " se prepare a partir du terminal "
				+ _service.getCurrentTerminal());
		_service.transferFrom(_target);//transfere des marchandises dans la flotte

		u.add(new EventArrivalFlotte(_service, Terminal.get(_service.getNextTerminal().replaceAll("[\\d+]", ""))),
				(_service.getNextDate() - (t % 14)));
		_service.moveToNextTerminal();

	}

}
