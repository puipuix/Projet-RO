package model;

/**
 * 
 * @author samuel
 *	
 * @param <T>
 * @param <U>
 */
public class Tuple<T, U> {
	public T a;
	public U b;

	public Tuple(T aa, U bb) {
		a = aa;
		b = bb;
	}
	
	@Override
	public String toString() {
	
		return "{ "+a.toString()+" , "+b.toString()+" }";
	}
}
