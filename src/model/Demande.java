package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * 
 * @author Alexis
 *
 */
public class Demande {
	public enum Etat {
		ATTENTE, TRANSPORT, LIVRE;
	}

	private Etat _etat;
	final private String _id;
	private String _path;
	private final String _origin, _destination;
	final private int _departure;
	final private int _arrival;
	final private int _volume;
	private int _late;

	public Demande(String _id, String _origin, String _destination, int _departure, int _arrival, int _volume) {
		super();
		_etat = Etat.ATTENTE;
		this._id = _id;
		this._origin = _origin;
		this._destination = _destination;
		this._departure = _departure;
		this._arrival = _arrival;
		this._volume = _volume;
		updatePath(_origin, _destination);
	}

	/**
	 * Create a path that will look like "A/0/C/1/D"
	 * 
	 * @param path
	 * @return
	 */
	public static String createPathString(Iterator<String> path) {
		StringBuilder sb = new StringBuilder();
		while (path.hasNext()) {
			sb.append(path.next());
			if (path.hasNext()) {
				sb.append("/");
			}
		}
		return sb.toString();
	}

	public static String createPathString(String... path) {
		return createPathString(Arrays.stream(path).iterator());
	}

	public static ArrayList<Demande> fromFile(String file) {
		ArrayList<Demande> list = new ArrayList<>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
			reader.readLine(); // Ignore first line

			while (reader.ready()) {
				String[] line = reader.readLine().split("\t");
				list.add(new Demande(line[0], line[1], line[2],
						Integer.parseInt(line[3]),
						Integer.parseInt(line[4]), Integer.parseInt(line[5])));
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("{").append(_id).append(" (").append(_etat);
		if (_late > 0) {
			sb.append("+").append(_late);
		}
		return sb.append(") [").append(_origin).append('>').append(_destination)
				.append(':').append(_path).append("][").append(_departure).append(',').append(_arrival).append("] ")
				.append(_volume).append('}').toString();
	}

	public String getId() {
		return _id;
	}

	public boolean isPathContainsStep(String from, String to) {
		return _path.contains(createPathString(from, to));
	}

	public void updatePath(String path) {
		_path = path;
	}

	public void updatePath(Iterator<String> path) {
		updatePath(createPathString(path));
	}

	public void updatePath(String... path) {
		updatePath(createPathString(path));
	}

	public String getPath() {
		return _path;
	}

	public String getOrigin() {
		return _origin;
	}

	public String getDestination() {
		return _destination;
	}

	public int getDeparture() {
		return _departure;
	}

	public int getArrival() {
		return _arrival;
	}

	public int getVolume() {
		return _volume;
	}

	public int getLate() {
		return _late;
	}

	public Etat getEtat() {
		return _etat;
	}
	
	public void setTransported() {
		_etat = Etat.TRANSPORT;
	}
	
	public void setDelivered(int late) {
		this._etat = Etat.LIVRE;
		_late = late;
	}

	@Override
	public int hashCode() {
		return _id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Demande) {
			Demande d = (Demande) obj;
			return _id == d._id;
		}
		return false;
	}
}
