package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * @author samuel
 *
 */
public class Service extends Inventory {

	public static ArrayList<Service> services;
	private final int serviceNum, load, unload, departureT, arrivalT;

	private final String origin;
	private final String destination;
	private final ArrayList<String> road;
	private final ArrayList<Tuple<String, Integer>> stop;
	private final ArrayList<Tuple<String, Integer>> boat;
	private int step = 0;

	private Service(int serviceNum, int capacity, int load, int unload, int departureT, int arrivalT, String origin,
			String destination, ArrayList<String> road, ArrayList<Tuple<String, Integer>> stop,
			ArrayList<Tuple<String, Integer>> boat) {
		super(serviceNum + "", capacity);
		this.serviceNum = serviceNum;
		this.load = load;
		this.unload = unload;
		this.departureT = departureT;
		this.arrivalT = arrivalT;
		this.origin = origin;
		this.destination = destination;
		this.road = road;
		this.stop = stop;
		this.boat = boat;
	}

	/**
	 * Permet la lecture d'un fichier de services
	 * @param src fichier d'entr�e
	 * @return
	 * @throws IOException
	 */
	public static ArrayList<Service> fromFile(String src) throws IOException {
		services = new ArrayList<>();
		BufferedReader reader;

		reader = new BufferedReader(new FileReader(src));
		String currentLine;
		reader.readLine();
		while (reader.ready()) {
			int id, capa, lo, unlo, depart, arrive;
			ArrayList<String> path = new ArrayList<>();
			ArrayList<Tuple<String, Integer>> boatList = new ArrayList<>();
			ArrayList<Tuple<String, Integer>> stopList = new ArrayList<>();
			currentLine = reader.readLine();
			String[] line = currentLine.split("/");
			// recuperation des parametres
			id = Integer.parseInt(line[0]);
			capa = Integer.parseInt(line[1]);
			lo = Integer.parseInt(line[3]);
			unlo = Integer.parseInt(line[4]);
			depart = Integer.parseInt(line[5]);
			arrive = Integer.parseInt(line[6]);

			// traitement du chemin
			String[] tmproad = line[2].split(";");
			for (String string : tmproad) {
				path.add(string.split(",")[0]);
			}
			System.out.println(currentLine);
			path.add(tmproad[tmproad.length - 1].split(",")[1]);

			// traitement stop
			String[] tmpStop = line[7].split(";");
			if (!tmpStop[0].equals("none")) {
				for (String string : tmpStop) {
					String[] oneStop = string.split(",");
					stopList.add(new Tuple<>(oneStop[0], Integer.parseInt(oneStop[1])));
				}
			}

			// traitement bateau
			for (String string : line[8].split(";")) {
				String[] oneBoat = string.split(",");
				boatList.add(new Tuple<>(oneBoat[0], Integer.parseInt(oneBoat[1])));

			}

			services.add(new Service(id, capa, lo, unlo, depart, arrive, path.get(0).charAt(0) + "",
					path.get(path.size() - 1).charAt(0) + "", path, stopList, boatList));

		}

		reader.close();

		return services;
	}

	/**
	 * retourne le terminal courant 
	 * @return
	 */
	public String getCurrentTerminal() {
		if (step >= road.size())
			return "an error occured";
		return road.get(step);
	}

	/**
	 * deplace le service au prochain terminal 
	 */
	public void moveToNextTerminal() {
		step++;
		if (step >= road.size())
			step = 0;
	}

	/**
	 * retourne la prochaine date a laquelle le service doit arriver
	 * @return
	 */
	public int getNextDate() {
		int value = 0;
		int index = (step + 1);

		if (index >= road.size()) {
			value = 14;
		} else {
			value = Integer.parseInt(road.get(index).replaceAll("\\D+", ""));
		}
		return value;
	}
	
	/**
	 * retourne le temps d'attente � un terminal
	 * @return
	 */
	public int waitingTime() {
		for (Tuple<String, Integer> tuple : stop) {
			if(tuple.a.equals(road.get(step))) {
				return tuple.b;
			}
		}		
		return 0;
	}

	public int getServiceNum() {
		return serviceNum;
	}

	public int getLoad() {
		return load;
	}

	public int getUnload() {
		return unload;
	}

	public int getDepartureT() {
		return departureT;
	}

	public int getArrivalT() {
		return arrivalT;
	}

	public String getOrigin() {
		return origin;
	}

	public String getDestination() {
		return destination;
	}

	public ArrayList<String> getRoad() {
		return road;
	}

	public ArrayList<Tuple<String, Integer>> getStop() {
		return stop;
	}

	public ArrayList<Tuple<String, Integer>> getBoat() {
		return boat;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String str = "\n" + "ID:" + serviceNum + "\n" + "\tcapacity:\n\torigin:" + origin + "\n\tdestination:"
				+ destination + "\n\tloading time:" + load + "\n\tunloading time:" + unload + "\n\tdeparture:"
				+ departureT + "\n\tarrival:" + arrivalT + "\n\troad:\n\t\t" + road + "\n\tstop:\n\t\t" + stop
				+ "\n\tvessel:\n\t\t" + boat;
		return str;
	}

	/**
	 * retourne le prochain terminal qui sera visit�
	 * @return
	 */
	public String getNextTerminal() {
		if(step+1>=road.size()) {
		
			return road.get(0);
		}
			
		return road.get(step+1);
	}

}
