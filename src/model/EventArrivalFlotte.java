package model;

import java.util.function.BiConsumer;

import utils.Log;

/**
 * 
 * @author samuel
 *
 */
public class EventArrivalFlotte implements BiConsumer<Integer, EventManager> {

	private final Service _service;
	private final Terminal _target;
		
	public EventArrivalFlotte(Service _service, Terminal _target) {
		super();
		this._service = _service;
		this._target = _target;
	}
	
	
	@Override
	public void accept(Integer t, EventManager u) {
	
		Log.println("Service "+_service.getServiceNum()+" arrive au terminal "+_service.getCurrentTerminal());
		_service.transferTo(_target);//transfere vers le terminal
		u.add(new EventDepartFlotte(_service, _target),_service.waitingTime());
		

	}

}
