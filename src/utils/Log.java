package utils;

public final class Log {

	private static StringBuilder _sb = new StringBuilder();
	
	public static void println(String text) {
		_sb.append(text).append('\n');
	}
	
	public static void print(String text) {
		_sb.append(text);
	}

	public static void flush() {
		System.out.print(_sb.toString());
		_sb = new StringBuilder();
	}
	
	public static boolean isReady() {
		return _sb.length() > 0;
	}
	
	public static boolean isEmpty() {
		return _sb.length() == 0;
	}
}
