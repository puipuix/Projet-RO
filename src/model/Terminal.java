package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;

import utils.Log;

/**
 * 
 * @author Alexis
 *
 */
public class Terminal extends Inventory {

	private static HashMap<String, Terminal> _map;

	public Terminal(String _id, int _capacity) {
		super(_id, _capacity);
	}

	public void Deliver(int currentHalfDay) {
		// remove all demands which have their destination set here, have all their TEU and with the correct time
		getInventory().keySet().removeIf(d ->{
			if (d.getDestination().equals(getId()) && d.getVolume() == getInventory().get(d) && currentHalfDay >= d.getArrival()) {
				d.setDelivered(currentHalfDay - d.getArrival());
				Log.println("Livraison de la demande: " + d.toString() + " avec " + d.getLate() + " demi-journee de retard.");
				return true;
			} else {
				return false;
			}
		});
	}

	public static HashMap<String, Terminal> fromFile(String file) {
		_map = new HashMap<>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(file)));
			reader.readLine(); // Ignore first line

			while (reader.ready()) {
				String[] line = reader.readLine().split("\t");
				_map.put(line[0], new Terminal(line[0], Integer.parseInt(line[1])));
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return _map;
	}

	public static Terminal get(String name) {
		return _map.get(name);
	}

	public static Collection<Terminal> getAll() {
		return _map.values();
	}
}
