package model;

import java.util.function.BiConsumer;

import utils.Log;

public class EventSpawnDemande implements BiConsumer<Integer, EventManager> {

	private final Demande _demande;
	private final Terminal _target;
		
	public EventSpawnDemande(Demande _demande, Terminal _target) {
		super();
		this._demande = _demande;
		this._target = _target;
	}

	@Override
	public void accept(Integer t, EventManager u) {
		Log.println("Arrivee de la demande : " + _demande);
		_target.add(_demande, _demande.getVolume(), true);
		_demande.setTransported();
	}

}
