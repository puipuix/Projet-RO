package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 
 * @author Alexis
 *
 */
public final class Simulation {

	public static final String LINE = "-".repeat(50);
	
	public static void Run(int sim_id, int ticks) throws IOException {
		EventLoop loop = new EventLoop();
		
		System.out.println("Reading files...");
		// load files
		ArrayList<Demande> demandes = Demande.fromFile(String.format("files/%d_demandes.txt", sim_id));
		ArrayList<Service> services = Service.fromFile(String.format("files/%d_services.txt", sim_id));
		HashMap<String, Terminal> terminaux = Terminal.fromFile(String.format("files/%d_topologie.txt", sim_id));
		
		System.out.println("Terminaux:\n" + terminaux + "\nDemandes:\n" + demandes + "\nServices:\n" + services);
		
		System.out.println("Loading graph and events...");
		// create paths
		Topologie topologie = new Topologie(services);
		for (Demande demande : demandes) {
			demande.updatePath(topologie.djikstra(demande.getOrigin(), demande.getDestination()).iterator());
			loop.getManager().add(new EventSpawnDemande(demande, terminaux.get(demande.getOrigin())), demande.getDeparture());
		}
		
		System.out.println("Paths:\n" + demandes);

		for (Service service : Service.services) {
			loop.getManager().add(new EventArrivalFlotte(service,Terminal.get(service.getCurrentTerminal().replaceAll("\\d+", ""))),0);
		}
		
		
		// run
		System.out.println("\n\nStarting simulation for " + ticks + " ticks.");
		long start = System.currentTimeMillis();
		
		
		for (int i = 0; i < ticks; i++) {
			loop.tick();
		}
		
		StringBuilder sb = new StringBuilder(LINE).append("\nEtat final:\n");
		demandes.forEach(d->sb.append(d.toString()).append('\n'));
		
		long time = System.currentTimeMillis() - start;
		long speed = time == 0 ? ticks : ticks / time * 1000;
		sb.append("Simulation ended after ").append(time).append("ms. ").append(speed).append("t/s");
		System.out.println(sb.toString());
	}
}
