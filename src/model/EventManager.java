package model;

import java.security.InvalidParameterException;
import java.util.ArrayDeque;
import java.util.function.BiConsumer;

/**
 * Store the events
 * @author Alexis
 *
 */
public class EventManager {

	private int _current_half_day;

	private ArrayDeque<BiConsumer<Integer, EventManager>>[] _half_days;

	public EventManager(int initial_capacity) {
		super();
		if (initial_capacity < 1) {
			throw new InvalidParameterException("Capacity can't be less than 1.");
		}
		_half_days = new ArrayDeque[initial_capacity];
		for (int i = 0; i < _half_days.length; i++) {
			_half_days[i] = new ArrayDeque<>();
		}
	}

	public EventManager() {
		this(1); // default capacity is 1
	}

	/**
	 * The numbers of ticks since the start of the simulation.
	 * 
	 * @return
	 */
	public int getCurrentHalfDay() {
		return _current_half_day;
	}

	/**
	 * Clear the current day events and increment the day counter.
	 */
	public void nextHalfDay() {
		getNow().clear(); // clear the event of the day
		_current_half_day++; // change to next day
	}

	/**
	 * Get all event for the current day
	 * 
	 * @return
	 */
	public ArrayDeque<BiConsumer<Integer, EventManager>> getNow() {
		return getHalfDay(_current_half_day);
	}

	/**
	 * Get all event for that day.
	 * 
	 * @param half_day
	 * @return
	 */
	public ArrayDeque<BiConsumer<Integer, EventManager>> getHalfDay(int half_day) {
		return _half_days[half_day % _half_days.length];
	}

	private void resize(int new_size) {
		//System.out.println("Resize to: " + new_size);
		ArrayDeque<BiConsumer<Integer, EventManager>>[] next = new ArrayDeque[new_size];
		// move the halfdays list to their new index.
		for (int i = 0; i < _half_days.length; i++) { // for all half day
			next[(_current_half_day + i) % new_size] = getHalfDay(_current_half_day + i); // from today to today + old size
		}
		// fill empty cell
		for (int i = 0; i < new_size; i++) {
			if (next[i] == null) {
				next[i] = new ArrayDeque<>();
			}
		}
		_half_days = next;
	}

	/**
	 * Add a new event that will be called in delay ticks.
	 * 
	 * @param event the event to call.
	 * @param delay
	 */
	public void add(BiConsumer<Integer, EventManager> event, int delay) {
		// if the delay is to long
		if (delay >= _half_days.length) {
			resize(delay + 1); // increase the capacity to fit that delay + 1 for today
		}
		getHalfDay(_current_half_day + delay).push(event);
	}
}
