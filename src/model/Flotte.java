package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * 
 * @author samuel
 *
 */
public class Flotte {

	private ArrayList<Tuple<String, Integer>> flotte;

	static public Flotte fromFile(String src) throws IOException {
		Flotte flotte = new Flotte();
		flotte.flotte = new ArrayList<>();
		String file = "";

		BufferedReader reader;

		reader = new BufferedReader(new FileReader(src));
		String currentLine;
		while (reader.ready()) {
			currentLine = reader.readLine();
			String[] tmp = currentLine.split(" ");
			flotte.flotte.add(new Tuple<String, Integer>(tmp[0], Integer.parseInt(tmp[1])));
		}

		reader.close();
		return flotte;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return flotte.toString();
	}
	
	private Flotte() {

	}

}
