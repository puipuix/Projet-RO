package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import utils.Log;

/**
 * 
 * @author Alexis
 *
 */
public abstract class Inventory {
	final private HashMap<Demande, Integer> _inventory = new HashMap<>();
	final private String _id;
	private int _size = 0;
	final private int _capacity;

	public Inventory(String _id, int _capacity) {
		super();
		this._id = _id;
		this._capacity = _capacity;
	}
	
	public String getId() {
		return _id;
	}

	public int getCapacity() {
		return _capacity;
	}

	public int size() {
		return _size;
	}
	
	public boolean isFull() {
		return getAviableCapacity() <= 0;
	}

	/**
	 * @return the volume that can be added
	 */
	public int getAviableCapacity() {
		if (_capacity < 0) {
			return Integer.MAX_VALUE;
		} else {
			return Math.max(0, _capacity - _size);
		}
	}
	
	public HashMap<Demande, Integer> getInventory(){
		return _inventory;
	}

	/**
	 * add c container to the inventory
	 * @param d
	 * @param c
	 * @return how many has been added
	 */
	public int add(Demande d, int c, boolean force) {
		int c2 = force ? c : Math.min(c, getAviableCapacity());
		if (c2 > 0) {
			_inventory.compute(d, (k, o) -> o == null ? c2 : o + c2); // if value don't we add it else we increase it
			_size += c2;
			Log.println("\tDeplacement de " + c2 + "TEU vers le " + getClass().getSimpleName().toLowerCase() + " " + getId() + " pour la demande : " + d.toString());
			return c2;
		} else {
			return 0;
		}
	}
	
	public int add(Demande d, int c) {
		return add(d, c, false);
	}

	/**
	 * 
	 * @param d
	 * @param c
	 * @return how many has been removed
	 */
	public int remove(Demande d, int c) {
		int[] pointer = { 0 }; // get int from elambda function, how many was removed
		_inventory.computeIfPresent(d, (k, o) -> {
			if (o <= c) {// if there is less that wanted
				pointer[0] = o; // remove all
				return null; // remove from map
			} else {
				pointer[0] = c;
				return o - c; // remove c
			}
		});
		_size -= pointer[0];
		return pointer[0];
	}

	/**
	 * transfer each demands that have "from/to" in their path
	 * @param from
	 * @param to
	 */
	public static void transfer(Inventory from, Inventory to) {
		ArrayList<Entry<Demande, Integer>> values = new ArrayList<>(from._inventory.entrySet());
		for (Entry<Demande, Integer> value : values) {
			if (to.isFull()) { // stop if to is full
				break;
			} else if (value.getKey().isPathContainsStep(from._id, to._id)) { // path contains "from/to" ?
				from.remove(value.getKey(), to.add(value.getKey(), value.getValue())); // remove as much as can be added
			}
		}
	}
	
	public void transferFrom(Inventory other) {
		transfer(other, this);
	}
	
	public void transferTo(Inventory other) {
		transfer(this, other);
	}
	
	@Override
	public int hashCode() {
		return _id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && getClass().isInstance(obj)) {
			Inventory t = (Inventory)obj;
			return t._id == _id;
		}
		return false;
	}

	@Override
	public String toString() {
		return "{" + _id + " " + _inventory.toString() + " " + _size + "/" + (_capacity < 0 ? "inf" : _capacity) + "}";
	}
}
