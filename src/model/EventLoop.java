package model;

import java.util.ArrayDeque;
import java.util.function.BiConsumer;

import utils.Log;

/**
 * 
 * @author Alexis
 *
 */
public class EventLoop {

	private EventManager _events = new EventManager();
	
	/**
	 * @return A manager to add new events.
	 */
	public EventManager getManager() {
		return _events;
	}
	
	/**
	 * Increment the day counter and call every event of that day.
	 */
	public void tick() {
		// do all event of the day
		ArrayDeque<BiConsumer<Integer, EventManager>> event =  _events.getNow();
		while (!event.isEmpty()) {
			event.pop().accept(_events.getCurrentHalfDay(), _events);
		}
		Terminal.getAll().forEach(ter->ter.Deliver(_events.getCurrentHalfDay())); // ask all terminal to deliver the containers
		
		if (Log.isReady()) { // if the current has print something
			System.out.println("Half day " + _events.getCurrentHalfDay() + ":");
			Log.println(Simulation.LINE);
			Log.flush();
		}
		_events.nextHalfDay();
	}
}
