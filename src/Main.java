import java.io.IOException;

import model.Simulation;

/**
 * 
 * @author Alexis
 *
 */
public class Main {
	
	public static void main(String[] args) throws IOException {
		int sim_id = 0;
		int ticks = 100 ;
		
		for (int i = 0; i < args.length-1; i +=2) {
			if (args[i].equals("--halfdays")) {
				ticks = Integer.parseInt(args[i+1]);
			} else if (args[i].equals("--id")) {
				sim_id = Integer.parseInt(args[i+1]);
			} else {
				System.err.println("Unknow argument: '" + args[i] + "'");
			}
		}
		
		Simulation.Run(sim_id, ticks);
	}

}
