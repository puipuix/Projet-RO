package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/***
 * 
 * @author samuel
 *
 */
public class Topologie {
	HashMap<String, ArrayList<Tuple<String, Integer>>> graph;
	ArrayList<Integer> distance;
	ArrayList<Tuple<String, String>> predecesseur;
	HashMap<String, Integer> toNumberList = new HashMap<>();

	/**
	 * cree une topologie (graphe contenant tous les deplacements possibles)
	 * @param services
	 */
	public Topologie(ArrayList<Service> services) {

		graph = new HashMap<>();

		for (Service service : services) {
			ArrayList<String> road = new ArrayList(service.getRoad());

			for (int i = 0; i < road.size(); i++) {
				road.set(i, road.get(i).replaceAll("\\d+", ""));
			}

			for (int i = 0; i < road.size() - 1; i++) {//on ajoute tout les terminaux en cle et les services qui y ont accees en valeur

				if (!graph.containsKey(road.get(i))) {
					ArrayList<Tuple<String, Integer>> path = new ArrayList<>();
					path.add(new Tuple<>(road.get(i + 1) + "", service.getServiceNum()));

					graph.put(road.get(i)+ "", path);
				} else {
					ArrayList<Tuple<String, Integer>> path = graph.get(road.get(i) + "");
					path.add(new Tuple<>(road.get(i + 1) + "", service.getServiceNum()));
					graph.replace(road.get(i) + "", path);
				}
			}
			graph.putIfAbsent(road.get(road.size() - 1), new ArrayList<>());
		}

		initToNumber(graph.keySet());//initialise la correspondance avec les terminaux

	}

	/**
	 * trouve tous les plus court chemins a partir de a
	 * @param a
	 */
	public void findPath(String a) {
		init(a);
		List<String> q = new ArrayList<>(graph.keySet());

		while (!q.isEmpty()) {
			String s1 = findMin(q);
			q.remove(s1);
			
			if(distance.get(toNumber(s1)) != Integer.MAX_VALUE) {
				for (Tuple<String, Integer> s2 : graph.get(s1)) {
					if (!s2.a.equals(s1)) {
						updateDistance(s1, new Tuple<>(s2.a, s2.b + ""));
					}

				}
			}
			
		}

	}

	/**
	 * trouve le plus court chemin entre 2 points
	 * @param a debut
	 * @param b fin
	 * @return
	 */
	public ArrayList<String> djikstra(String a, String b) {
	

		findPath(a);
		ArrayList<String> path = new ArrayList<>();
		path.add(b);
		Tuple<String, String> s = predecesseur.get(toNumber(b));

		while (s != null && !s.a.equals(a)) {
			path.add(s.b);
			path.add(s.a);
			s = predecesseur.get(toNumber(s.a));
		}
		if (s != null) {
			path.add(s.b);
			path.add(s.a);
		}

		Collections.reverse(path);//on remet le chemin dans le bon sens
		
		ArrayList<String> filtered = new ArrayList<>();		//filtrage du path afin de faciliter la lecture et reduire le chemin
		for (int i = 0; i < path.size(); i++){
		  filtered.add(path.get(i));
		  for (int j = i + 2; j < path.size(); j += 2) {
		    if (path.get(i).equals(path.get(j))){
		      i=j;
		    } else {
		      break;
		    }
		  }
		}
		return filtered;
	}

	/**
	 * trouve le terminal le plus proche
	 * @param q
	 * @return
	 */
	public String findMin(List<String> q) {
	
		int dist = Integer.MAX_VALUE;
		String sommet = "";
		for (String s : q) {

			if (distance.get(toNumber(s)) <= dist) {
				dist = distance.get(toNumber(s));
				sommet = s;
			}
		}
		return sommet;

	}

	/**
	 * mise a jour des distance entre les terminaux
	 * @param s1
	 * @param s2
	 */
	public void updateDistance(String s1, Tuple<String, String> s2) {

		if (distance.get(toNumber(s2.a)) > (distance.get(toNumber(s1)) + weight(s1, s2.a))) {
			distance.set(toNumber(s2.a), distance.get(toNumber(s1)) + weight(s1, s2.a));
			predecesseur.set(toNumber(s2.a), new Tuple<>(s1, s2.b));
		}
	}

	/**
	 * initialise le dictionnaire de sommet
	 * @param s
	 */
	public void initToNumber(Set<String> s) {
		int i = 0;
		for (String string : s) {
			toNumberList.put(string, i++);
		}
	}

	/**
	 * renvoi l'equivalent d'un sommet
	 * @param s
	 * @return
	 */
	public int toNumber(String s) {
		return toNumberList.get(s);

	}

	/**
	 * initialise djikstra
	 * @param deb
	 */
	private void init(String deb) {

		distance = new ArrayList<>();
		predecesseur = new ArrayList<>();
		for (int i = 0; i < graph.keySet().size(); i++) {

			distance.add(Integer.MAX_VALUE);
			predecesseur.add(null);
		}

		distance.set(toNumber(deb), 0);
	}

	/**
	 * retourne le poids entre deux sommets
	 * @param s1
	 * @param s2
	 * @return
	 */
	public int weight(String s1, String s2) {
		return 1;
	}

}
